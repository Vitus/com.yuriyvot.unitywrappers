namespace YuriyVot.UnityWrappers
{
    public interface IRandomProvider
    {
        int Range (int min, int max);
        float Range (float min, float max);
        float Next ();
    }
}
