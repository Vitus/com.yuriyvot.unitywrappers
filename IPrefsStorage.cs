namespace YuriyVot.UnityWrappers
{
    public interface IPrefsStorage
    {
        void DeleteKey (string key);
        void DeleteAll ();
        bool HasKey (string key);
        int GetInt (string key, int defaultValue = default);
        void SetInt (string key, int value);
        float GetFloat (string key, float defaultValue = default);
        void SetFloat (string key, float value);
        string GetString (string key, string defaultValue = default);
        void SetString (string key, string value);
    }
}
