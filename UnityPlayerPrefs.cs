using UnityEngine;

namespace YuriyVot.UnityWrappers
{
    public class UnityPlayerPrefs : IPrefsStorage
    {
        public static readonly UnityPlayerPrefs Instance = new UnityPlayerPrefs ();

        public void DeleteKey (string key)
            => PlayerPrefs.DeleteKey (key);

        public void DeleteAll ()
            => PlayerPrefs.DeleteAll ();

        public bool HasKey (string key)
            => PlayerPrefs.HasKey (key);

        public int GetInt (string key, int defaultValue = default)
            => PlayerPrefs.GetInt (key, defaultValue);

        public void SetInt (string key, int value)
            => PlayerPrefs.SetInt (key, value);

        public float GetFloat (string key, float defaultValue = default)
            => PlayerPrefs.GetFloat (key, defaultValue);

        public void SetFloat (string key, float value)
            => PlayerPrefs.SetFloat (key, value);

        public string GetString (string key, string defaultValue = default)
            => PlayerPrefs.GetString (key, defaultValue);

        public void SetString (string key, string value)
            => PlayerPrefs.SetString (key, value);
    }
}
