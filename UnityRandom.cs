﻿using UnityEngine;

namespace YuriyVot.UnityWrappers
{
    public class UnityRandom : IRandomProvider
    {
        public static readonly UnityRandom Instance = new UnityRandom ();

        public int Range (int min, int max)
            => Random.Range (min, max);

        public float Range (float min, float max)
            => Random.Range (min, max);

        public float Next ()
            => Random.value;
    }
}
